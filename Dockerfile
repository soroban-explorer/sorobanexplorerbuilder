# Use the official Rust base image
ARG rustVersion
ARG sorobanVersion

FROM rust:${rustVersion}

WORKDIR /app
RUN rustup target add wasm32-unknown-unknown
RUN cargo install --locked --version ${sorobanVersion} --features opt soroban-cli

COPY . .

RUN cargo clean
RUN cargo build --release

CMD ["./target/release/soroban-contract-explorer-builder"]