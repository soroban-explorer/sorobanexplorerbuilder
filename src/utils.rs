pub fn error_msg(msg1: &str, msg2:String) -> String {
    let msg = format!("Error: {} - {}", msg1, msg2);
    msg
}