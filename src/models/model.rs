use serde::{Deserialize, Serialize};


#[derive(Deserialize, Serialize)]
pub struct Builder {
    pub rust: String,
    pub soroban: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct SourceRequest {
    pub id: i64,
    pub script: String,
    pub contract_id: String,
    pub rust_version: String,
    pub file_path: String,
    pub explorer_data_id: String,
    pub wasm_name: String,
    pub wasm_hash: String,
    pub network: String,
    pub account: String,
    pub callback_svc_url: String,
    pub wasm_compile_file: String,
    pub structure: Option<String>,
    pub use_make_file: bool

}