use actix_cors::Cors;
use actix_web::middleware::Logger;
use actix_web::{ App, HttpServer};
use dotenv::dotenv;
use log::{ info };
mod utils;
mod routes;
mod models;
mod svc;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    if std::env::var_os("RUST_LOG").is_none() {
        std::env::set_var("RUST_LOG", "info"); // Set the default log level to include info for all crates
    }
    dotenv().ok();
    //env_logger::init();
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    let bind_address = std::env::var("BIND_ADDRESS").expect("BIND_ADDRESS is not set");
    info!("🚀 Server started successfully @{:?}", bind_address);
    HttpServer::new(move || {
        let cors = Cors::permissive();
        App::new()
            .configure(routes::config)
            .wrap(cors)
            .wrap(Logger::default())
    })
        .bind(&bind_address)?
        .run()
        .await
}
