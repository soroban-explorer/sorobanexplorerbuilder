use serde_json::json;
use actix_web::{ post, web, HttpResponse, Responder};
use crate::svc::local_build_svc:: { process_contract_local_build };
use crate::models::model:: { SourceRequest };
use log::{ info, error};

/// Handles the HTTP POST request to build a contract locally.
///
/// # Arguments
///
/// * `source` - JSON payload containing source request information.
///
/// # Returns
///
/// Returns an `impl Responder` representing the HTTP response.
#[post("/local")]
pub async fn build_contract_local(
    source: web::Json<SourceRequest>
) -> impl Responder {
    info!("------ process_contract_local_build: id: {:?}, explorer_data_id: {:?}, file_path: {}, callback: {}, network: {}", source.id, source.explorer_data_id, source.file_path, source.callback_svc_url, source.network);

    match process_contract_local_build(&source.id, &source.explorer_data_id, &source.file_path,  &source.wasm_name,  &source.contract_id,  &source.network, &source.callback_svc_url,  &source.account,  &source.structure, &source.use_make_file) {
        Ok(result) => {
            info!(" ----- success process contract local build, ID:{:?}, explorer_data_id: {}, file path: {}. ---->", source.id, source.explorer_data_id, source.file_path);
            HttpResponse::Ok().json(json!({"status": true, "id": source.id, "wasmhash": result}))
        }
        Err(_err) => {
            // If build fails, handle the error'
            let msg = format!("error process contract local build, ID:{:?}, explorer_data_id: {}, file path: {}.", source.id, source.explorer_data_id, source.file_path);
            error!("{:?}", msg);
            HttpResponse::Ok().json(json!({"status": false, "id": source.id, "wasmhash": ""}))
        }
    }
}