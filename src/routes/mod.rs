pub mod local_builder_route;


use crate::routes::local_builder_route::{build_contract_local, health};

use actix_web::{web};

pub(crate) fn config(app: &mut web::ServiceConfig) {
    app.service(
        web::scope("/api")
            // process routes
            .service(
                web::scope("/build")
                    .service(build_contract_local)
            ),
    );
}