use std::process::Command;
use crate::utils::error_msg;
use std::panic;
use log::{ info, error};

/// Processes the local build of a contract.
///
/// # Arguments
///
/// * `id` - The identifier of the contract.
/// * `_explorer_data_id` - The data identifier of the explorer.
/// * `file_path` - The path to the contract file.
/// * `wasm_name` - The name of the WebAssembly file.
/// * `contract_id` - The ID of the contract.
/// * `network` - The network information.
/// * `callback_svc_url` - The callback service URL.
/// * `account` - The account information.
/// * `structure` - The contract structure (optional).
/// * `use_make_file` - Indicates whether to use a Makefile for building.
///
/// # Returns
///
/// Returns `Ok(())` if the local build action is successful, otherwise returns an error message.
pub fn process_contract_local_build(
    id:  &i64, _explorer_data_id: &str, file_path: &str,  wasm_name: &str,  wasm_hash: &str, contract_id: &str,  network: &str,  callback_svc_url: &str, account: &str,  structure: &Option<String>, use_make_file: &bool, rust_version: &str
) -> Result<(), String> {
    info!("------ starting process contract local build action, id: {:?}, file path: {:?}, rust: {:?}, wasm_hash: {:?}",id, file_path, rust_version, wasm_hash);
    let script = std::env::var("BUILD_SCRIPT_PATH").expect("BUILD_SCRIPT_PATH must be set");
    info!("------ build script: {:?}", script);
    let make_build = if *use_make_file { "true"} else { "false"};
    match perform_contract_local_build_action(*id, script.to_string(), file_path.to_string(), wasm_name.to_string(), wasm_hash.to_string(), contract_id.to_string(), network.to_string(),  callback_svc_url.to_string(), account.to_string(), structure.clone(), make_build.to_string(), rust_version.to_string()) {
        Ok(result) => {
            info!(" ----- success process contract local build action, id: {:?}, file path: {:?}", id, file_path);
            return Ok(result);
        }
        Err(err) => {
            let msg = format!("id: {:?}, file path: {},  {:?}", id, file_path, err);
            error!("{:?}", msg);
            Err(error_msg("Error process contract local build", msg))
        }
    }
}
/// Performs the local build action for a contract.
///
/// # Arguments
///
/// * `id` - The identifier of the contract.
/// * `script` - The script for the build action.
/// * `file_path` - The path to the contract file.
/// * `wasm_name` - The name of the WebAssembly file.
/// * `contract_id` - The ID of the contract.
/// * `network` - The network information.
/// * `callback_svc_url` - The callback service URL.
/// * `account` - The account information.
/// * `_structure` - The contract structure (optional).
/// * `use_make_file` - Indicates whether to use a Makefile for building.
///
/// # Returns
///
/// Returns `Ok(())` if the local build action is successful, otherwise returns an error message.
pub  fn perform_contract_local_build_action(id: i64, script: String, file_path: String, wasm_name:String, wasm_hash:String, contract_id:String, network:String, callback_svc_url: String, account: String, _structure: Option<String>, use_make_file: String, rust_version: String) -> Result<(), String>  {
    info!("------ process contract local build action, id: {:?}, 1. project_path: {:?} 2. wasm_name {:?},3.  contract_id {:?},  4. explorer_id {:?}, 5. url: {:?} 6. account {:?}  7. use_make_file: {:?}, script {:?}, 8 network {:?}, 9: rust: {:?}, 10: expected wasm_hash: {:?}", id, file_path, wasm_name, contract_id, id, callback_svc_url, account, use_make_file, script, network, rust_version, wasm_hash);
    let project_path = file_path.clone();
    let result = panic::catch_unwind(|| {
        Command::new(script)
            .arg(project_path)
            .arg(wasm_name)
            .arg(contract_id)
            .arg(id.to_string())
            .arg(callback_svc_url)
            .arg(account)
            .arg(use_make_file)
            .arg(network)
            .arg(rust_version)
            .arg(wasm_hash)
            .output().unwrap();
    });
    match result {
        Ok(_res) => {
            info!("processed contract local build action, check wasm file  id: {:?}", id);
            return Ok(());
        },
        Err(_) => {
            error!("caught panic for local build action!");
            //return Err(Error::new(ErrorKind::Other, format!("panic, failed to process: {}", file_path)))
        },
    }
    let msg = format!("operation file path: {}.", file_path.clone());
    Err(error_msg("other, failed to process", msg))
}